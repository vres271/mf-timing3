import { RaceDTO } from '../models/race.model';
export const RacesMockData: RaceDTO[] = [
  {id: 1, name: "Медицинка XCO", startDate: 1685250000000, raceType: 1},
  {name: "LCPF 2024 - Левобережный ПампФест", startDate: 1719532800000, raceType: 2, id: 2},
  {id: 3, name: "Малиновый XCO", startDate: 1693724478000, raceType: 1},
]


